package com.crud.utils;

public enum TipoDocumento {
	DNI, PASAPORTE, CARNET_EXTRANJERIA
}
