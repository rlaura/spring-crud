package com.crud.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.crud.entity.Cliente;
import com.crud.service.ClienteService;

import lombok.RequiredArgsConstructor;

@RequestMapping(ClienteController.CLIENTES)
@RestController
@RequiredArgsConstructor
public class ClienteController {
	public static final String CLIENTES = "/clientes";

	@Autowired
	private ClienteService clienteService;

	@PostMapping("/save")
	public ResponseEntity<Cliente> guardar(@RequestBody Cliente cliente){
		return new ResponseEntity<>(clienteService.guardar(cliente),HttpStatus.CREATED);
	}
	
	@GetMapping("/all")
	public ResponseEntity<List<Cliente>> listar(){
		return new ResponseEntity<List<Cliente>>(clienteService.listar(),HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Cliente> buscarPorId(@PathVariable Long id) {
		return new ResponseEntity<>(clienteService.buscarPorCodigo(id),HttpStatus.OK);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Cliente> actualizarPorId(@PathVariable Long id, @RequestBody Cliente cliente) {
		return new ResponseEntity<>(clienteService.actualizar(id, cliente),HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity borrar(@PathVariable Long id) {
		clienteService.borrar(id);
		return new ResponseEntity(HttpStatus.OK);
	}
	
	/**
	 * https://medium.com/@raviyasas/spring-boot-best-practices-for-developers-3f3bdffa0090
	 * https://medium.com/learnwithnk/best-practices-in-spring-boot-project-structure-layers-of-microservice-versioning-in-api-cadf62bd3459
	 * https://salithachathuranga94.medium.com/validation-and-exception-handling-in-spring-boot-51597b580ffd
	 * https://medium.com/@sampathsl/exception-handling-for-rest-api-with-spring-boot-c5d5ba928f5b
	 * 
	 */

}
