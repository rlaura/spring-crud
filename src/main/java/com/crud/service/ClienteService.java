package com.crud.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crud.entity.Cliente;
import com.crud.repository.ClienteRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class ClienteService {

	@Autowired
	private ClienteRepository clienteRepository;
	//R
	public List<Cliente> listar() {
		return clienteRepository.findAll();
	}
	//C - U
	public Cliente guardar(Cliente cliente) {
		return clienteRepository.save(cliente);
	}

	//C - U
	public Cliente actualizar(Long id, Cliente clienteRequest) {
		Cliente existeCliente = clienteRepository.findById(id).get();
		existeCliente.setNombre(clienteRequest.getNombre());
		existeCliente.setApellidos(clienteRequest.getApellidos());
		existeCliente.setTipoDocumento(clienteRequest.getTipoDocumento());
		existeCliente.setNumeroDocumento(clienteRequest.getNumeroDocumento());
		return clienteRepository.save(existeCliente);
	}

	//R
	public Cliente buscarPorCodigo(Long id) {
		// usar getReferenceById y no getById(ID) porque esta deprecado
		//usamos getById cuando usamos una session entre ida y vuleta y es un metodo lazy
		//return clienteRepository.getReferenceById(id);
		return clienteRepository.findById(id).get();
	}
	//D
	public void borrar(Long id) {
		clienteRepository.deleteById(id);
	}
}
